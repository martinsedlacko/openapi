package com.aardwark.OpenAPI.controllers;

import com.aardwark.OpenAPI.model.Employee;
import com.aardwark.OpenAPI.payload.ResponseMessage;
import com.aardwark.OpenAPI.repositories.EmployeeRepository;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class EmployeeController {

    public static final String EMPLOYEE_NOT_FOUND = "Employee not found";
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = Objects.requireNonNull(employeeRepository
                , "EmployeeRepository must be provided");
    }

    @GetMapping( value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllEmployees(){
        return new ResponseEntity<>(employeeRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping( value = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployeeById(@RequestParam int id) throws NotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent())
            return new ResponseEntity<>(employee.get(), HttpStatus.OK);
        else
            throw new NotFoundException(EMPLOYEE_NOT_FOUND);
    }

    @ResponseBody
    @PostMapping(value = "/employee", produces = {MediaType.APPLICATION_JSON_VALUE}
                , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> addEmployee(@Valid @RequestBody Employee employee)   {
        return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
    }

    @ResponseBody
    @ApiOperation("deleteEmployeeById, if employee is headOfDepartment, first delete/modify department")
    @DeleteMapping(value = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> deleteEmployeeById(@RequestParam int id) throws NotFoundException {
        if(employeeRepository.existsById(id)){
            employeeRepository.deleteById(id);
            return new ResponseEntity<>(new ResponseMessage(ResponseMessage.SUCCESSFULLY_DELETED
                    , HttpStatus.OK.toString())
                    , HttpStatus.OK);
        }else
            throw new NotFoundException(EMPLOYEE_NOT_FOUND);
    }

    @ResponseBody
    @PutMapping(value = "/employee")
    public ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee) throws NotFoundException {
        if(employeeRepository.existsById(employee.getId()))
            return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
        else
            throw new NotFoundException(EMPLOYEE_NOT_FOUND);
    }
}
