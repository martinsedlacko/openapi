package com.aardwark.OpenAPI.controllers;

import com.aardwark.OpenAPI.model.Department;
import com.aardwark.OpenAPI.payload.ResponseMessage;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.aardwark.OpenAPI.repositories.DepartmentRepository;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class DepartmentController {

    public static final String DEPARTMENT_NOT_FOUND = "Department not found";

    private final DepartmentRepository departmentRepository;

    public DepartmentController(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @ResponseBody
    @PostMapping(value = "/department", produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "addDepartment, also add new Employee if head of department is not in employee table(new Employee)")
    public ResponseEntity<Department> addDepartment(@Valid @RequestBody Department department) {
        return new ResponseEntity<>(departmentRepository.save(department), HttpStatus.OK);
    }

    @ResponseBody
    @DeleteMapping(value = "/department", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> deleteDepartmentById(@RequestParam int id) throws NotFoundException {
        if(departmentRepository.existsById(id))
        {
            departmentRepository.deleteById(id);
            return new ResponseEntity<>(new ResponseMessage(ResponseMessage.SUCCESSFULLY_DELETED,
                    HttpStatus.OK.toString()),HttpStatus.OK);
        }else
            throw new NotFoundException(DEPARTMENT_NOT_FOUND);
    }

    @ResponseBody
    @PutMapping(value = "/department", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Department> updateDepartment(@Valid @RequestBody Department department) throws NotFoundException {
        if(departmentRepository.existsById(department.getId()))
        {
            return new ResponseEntity<>( departmentRepository.save(department), HttpStatus.OK);
        }else
            throw new NotFoundException(DEPARTMENT_NOT_FOUND);
    }

    @ResponseBody
    @GetMapping(value = "/department", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Department> getDepartmentById(@RequestParam int id) throws NotFoundException {
            return new ResponseEntity<>( departmentRepository.findById(id).orElseThrow(
                    () -> new NotFoundException(DEPARTMENT_NOT_FOUND)
            ), HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/departments", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Department>> getAllDepartments(){
        return new ResponseEntity<>(departmentRepository.findAll(), HttpStatus.OK);
    }
}
