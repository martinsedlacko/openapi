package com.aardwark.OpenAPI.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessage {
    public static final String SUCCESSFULLY_DELETED = "Successfully deleted";
    private String message;
    private String status;
}
