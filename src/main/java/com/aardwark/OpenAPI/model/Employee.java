package com.aardwark.OpenAPI.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotBlank
    @Size(min = 3, max = 64)
    private String firstName;

    @NotBlank
    @Size(min = 3, max = 64)
    private String lastName;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    @Pattern(regexp = "(\\+421)\\s*([\\d]{3})\\s*([\\d]{3})\\s*([\\d]{3})$", message = "Must be in \"+421 xxx xxx xxx\" format")
    @ApiModelProperty(value = "Must be in \"+421 xxx xxx xxx\" format", required = true)
    private String telephoneNumber;

    @NotNull
    @PositiveOrZero
    private BigDecimal salary;
}
