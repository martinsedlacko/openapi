package com.aardwark.OpenAPI.controllers;

import com.aardwark.OpenAPI.payload.ResponseMessage;
import com.aardwark.OpenAPI.utils.HttpUtils;
import javassist.NotFoundException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.aardwark.OpenAPI.model.Employee;
import com.aardwark.OpenAPI.repositories.EmployeeRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {

    @Mock
    private EmployeeRepository employeeRepository;

    private EmployeeController employeeController;

    private final List<Employee> employeeList = new ArrayList<>();

    private final String INVALID_JSON = "{" + "\"firstName\"  : " + "\"Ma\" ," +
            " \"telephoneNumber\" : " + null + "}" ;

    @Before
    public void setUp()  {
        employeeController = new EmployeeController(employeeRepository);

        employeeList.add(new Employee(
                1,
                "Martin",
                "Mrkva",
                "martin@mrkva.com",
                "+421907379336",
                new BigDecimal("500")
        ));

        employeeList.add(new Employee(
                2,
                "Jozef",
                "Mrkva",
                "jozko@mrkva.com",
                "+421907379336",
                new BigDecimal("500")
        ));
    }

    @Test
    public void getEmployeeByIdWithValidIdShouldReturnEmployeeWithStatus200() throws NotFoundException {
        when(employeeRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(employeeList.get(0)));

        ResponseEntity<Employee> responseEntity = employeeController.getEmployeeById(1);

        Employee employee = responseEntity.getBody();

        assertAll(
                () -> {
                    assert employee != null;
                    assertEquals(employeeList.get(0).getId(),employee.getId());
                },
                () -> {
                    assert employee != null;
                    assertEquals(employeeList.get(0).getFirstName(),employee.getFirstName());
                },
                () -> {
                    assert employee != null;
                    assertEquals(0, employee.getSalary().compareTo(employeeList.get(0).getSalary()));
                }
        );

        verify(employeeRepository, times(1)).findById(1);
    }

    @Test
    public void getEmployeeByIdWithInvalidIdShouldReturnNotFoundResponse()  {
        when(employeeRepository.findById(1)).thenReturn(Optional.empty());

        NotFoundException exception = assertThrows(NotFoundException.class,
                () -> employeeController.getEmployeeById(1));

        assertEquals(EmployeeController.EMPLOYEE_NOT_FOUND, exception.getMessage());

        verify(employeeRepository, times(1)).findById(1);
    }

    @Test
    public void addEmployeeWithValidValuesShouldReturnNewEmployeeWithStatus200(){
        Employee employee = employeeList.get(0);

        when(employeeRepository.save(employee)).thenReturn(employee);

        ResponseEntity<Employee> responseEntity = employeeController.addEmployee(employee);

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(employee.getFirstName(),
                        Objects.requireNonNull(responseEntity.getBody()).getFirstName()),
                () -> assertEquals(0, employee.getSalary().compareTo(responseEntity.getBody().getSalary()))
        );

        verify(employeeRepository, times(1)).save(employee);
    }

    @Test
    public void addEmployeeWithInvalidValuesShouldReturnBadRequest() throws IOException {
        HttpPost httpPost = new HttpPost(HttpUtils.BASE_ENDPOINT + "employee");

        CloseableHttpClient client = HttpClientBuilder.create().build();

        httpPost.setEntity(new StringEntity(INVALID_JSON, ContentType.APPLICATION_JSON));

        CloseableHttpResponse response = client.execute(httpPost);

        int status = response.getStatusLine().getStatusCode();

        assertEquals(HttpStatus.BAD_REQUEST.value(), status);

        client.close();
        response.close();
    }

    @Test
    public void updateEmployeeWithValidValuesShouldReturnNewEmployeeWithStatus200 () throws NotFoundException {
        Employee employee = employeeList.get(0);
        when(employeeRepository.existsById(1)).thenReturn(true);
        when(employeeRepository.save(employee)).thenReturn(employee);

        ResponseEntity<Employee> responseEntity = employeeController.updateEmployee(employee);

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(employee.getId(), Objects.requireNonNull(responseEntity.getBody()).getId()),
                () -> assertEquals(employee.getFirstName(), responseEntity.getBody().getFirstName())
        );

        verify(employeeRepository, times(1)).existsById(1);
        verify(employeeRepository, times(1)).save(employee);
    }

    @Test
    public void updateEmployeeWithInvalidIdValueShouldReturnNotFound ()  {
        Employee employee = employeeList.get(0);
        when(employeeRepository.existsById(1)).thenReturn(false);

        NotFoundException notFoundException = assertThrows(NotFoundException.class,
                () -> employeeController.updateEmployee(employee));

        assertEquals(EmployeeController.EMPLOYEE_NOT_FOUND, notFoundException.getMessage());

        verify(employeeRepository,times(1)).existsById(1);
    }

    @Test
    public void updateEmployeeWithInvalidValuesShouldReturnBadRequest() throws IOException {
        HttpPut httpPut = new HttpPut(HttpUtils.BASE_ENDPOINT + "employee" );

        CloseableHttpClient client = HttpClientBuilder.create().build();

        httpPut.setEntity(new StringEntity(INVALID_JSON, ContentType.APPLICATION_JSON));

        CloseableHttpResponse response = client.execute(httpPut);

        int status = response.getStatusLine().getStatusCode();

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST.value(), status)
        );
    }

    @Test
    public void deleteEmployeeByIdWithValidIdShouldReturnStatus200() throws NotFoundException {
        when(employeeRepository.existsById(1)).thenReturn(true);

        ResponseEntity<ResponseMessage> responseEntity = employeeController.deleteEmployeeById(1);

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(ResponseMessage.SUCCESSFULLY_DELETED,
                        Objects.requireNonNull(responseEntity.getBody()).getMessage()),
                () -> assertEquals(HttpStatus.OK.toString(),
                        Objects.requireNonNull(responseEntity.getBody()).getStatus())
        );

        verify(employeeRepository,times(1)).existsById(1);
    }

    @Test
    public void deleteEmployeeByIdWithInvalidIdShouldThrowNotFoundException() {
        when(employeeRepository.existsById(1)).thenReturn(false);

        NotFoundException exception = assertThrows(NotFoundException.class,
                () -> employeeController.deleteEmployeeById(1));

        assertEquals(EmployeeController.EMPLOYEE_NOT_FOUND, exception.getMessage());

        verify(employeeRepository,times(1)).existsById(1);
    }

    @Test
    public void getAllEmployeesShouldReturnListOfEmployeesWithStatus200(){
        when(employeeRepository.findAll()).thenReturn(employeeList);

        ResponseEntity<List<Employee>> responseEntity = employeeController.getAllEmployees();

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(employeeList.get(0)
                        , Objects.requireNonNull(responseEntity.getBody()).get(0)),
                () -> assertEquals(employeeList.get(1)
                        , Objects.requireNonNull(responseEntity.getBody()).get(1))
        );

        verify(employeeRepository, times(1)).findAll();
    }

}
