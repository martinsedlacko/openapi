package com.aardwark.OpenAPI.controllers;

import com.aardwark.OpenAPI.model.Department;
import com.aardwark.OpenAPI.model.Employee;
import com.aardwark.OpenAPI.payload.ResponseMessage;
import com.aardwark.OpenAPI.repositories.DepartmentRepository;
import com.aardwark.OpenAPI.utils.HttpUtils;
import javassist.NotFoundException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentControllerTest {

    @Mock
    private DepartmentRepository departmentRepository;

    private DepartmentController departmentController;

    private final List<Department> departmentList = new ArrayList<>();

    private final String INVALID_JSON = "{" +
            "\"name\" : " + null +
            "}";

    @Before
    public void setUp() {
        departmentController = new DepartmentController(departmentRepository);
        departmentList.add(new Department(1, "Development", new Employee(
                1,
                "Martin",
                "Mrkva",
                "martin@mrkva.com",
                "+421907379336",
                new BigDecimal("500")
        )));

        departmentList.add(new Department(2, "Marketing", new Employee(
                2,
                "Jozef",
                "Mrkva",
                "jozef@mrkva.com",
                "+421907379336",
                new BigDecimal("750")
        )));
    }

    @Test
    public void getAllDepartmentsShouldReturnDepartmentListWithStatus200(){

        when(departmentRepository.findAll()).thenReturn(departmentList);

        ResponseEntity<List<Department>> response = departmentController.getAllDepartments();

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(departmentList.get(0), Objects.requireNonNull(response.getBody()).get(0))
        );

        verify(departmentRepository, times(1)).findAll();
    }

    @Test
    public void getDepartmentByIdGivenCorrectIdShouldReturnDepartmentWithStatus200() throws NotFoundException {
        when(departmentRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(departmentList.get(0)));

        ResponseEntity<Department> responseEntity = departmentController.getDepartmentById(1);

        Department department = responseEntity.getBody();

        assertAll(
                () -> {
                    assert department != null;
                    assertEquals(departmentList.get(0).getId(),department.getId());
                },
                () -> {
                    assert department != null;
                    assertEquals(departmentList.get(0).getName(),department.getName());
                },
                () -> {
                    assert department != null;
                    assertEquals(departmentList.get(0).getHeadOfDepartment().getId()
                            ,department.getHeadOfDepartment().getId());
                }
        );
        verify(departmentRepository, times(1)).findById(1);
    }

    @Test
    public void getDepartmentByIdGivenIncorrectIdShouldReturnStatus404()  {
        when(departmentRepository.findById(10)).thenReturn(Optional.empty());


        NotFoundException thrownException = assertThrows (NotFoundException.class,
                () -> departmentController.getDepartmentById(10));

        assertEquals(DepartmentController.DEPARTMENT_NOT_FOUND,thrownException.getMessage());

        verify(departmentRepository, times(1)).findById(10);
    }

    @Test
    public void addDepartmentWithValidValuesShouldReturnNewDepartmentWithStatus200(){
        when(departmentRepository.save(departmentList.get(0))).thenReturn(departmentList.get(0));

        ResponseEntity<Department> responseEntity = departmentController.addDepartment(departmentList.get(0));

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(departmentList.get(0).getId(), Objects.requireNonNull(responseEntity.getBody()).getId()),
                () -> assertEquals(departmentList.get(0).getName(), responseEntity.getBody().getName())
        );

        verify(departmentRepository, times(1)).save(departmentList.get(0));
    }

    @Test
    public void addDepartmentWithInvalidValuesShouldReturnBadRequest() throws IOException {
        HttpPost httpPost = new HttpPost(HttpUtils.BASE_ENDPOINT + "department");

        CloseableHttpClient client = HttpClientBuilder.create().build();

        httpPost.setEntity(new StringEntity(INVALID_JSON, ContentType.APPLICATION_JSON));

        CloseableHttpResponse response = client.execute(httpPost);

        int status = response.getStatusLine().getStatusCode();

        assertEquals(HttpStatus.BAD_REQUEST.value(), status);

        client.close();
        response.close();
    }

    @Test
    public void updateDepartmentWithValidValuesShouldReturnUpdatedDepartmentWithStatus200() throws NotFoundException {
        Department department = departmentList.get(0);
        department.setName("Updated");

        when(departmentRepository.existsById(department.getId())).thenReturn(true);
        when(departmentRepository.save(department)).thenReturn(department);

        ResponseEntity<Department> responseEntity = departmentController.updateDepartment(department);

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(department.getName(), Objects.requireNonNull(responseEntity.getBody()).getName())
        );

        verify(departmentRepository, times(1)).existsById(department.getId());
        verify(departmentRepository, times(1)).save(department);
    }

    @Test
    public void updateDepartmentWithInvalidIdValuesShouldReturnStatus404(){
        Department department = departmentList.get(0);
        department.setId(15);

        when(departmentRepository.existsById(department.getId())).thenReturn(false);

        NotFoundException thrownException = assertThrows(NotFoundException.class,
                () -> departmentController.updateDepartment(department)
        );

        assertEquals(DepartmentController.DEPARTMENT_NOT_FOUND, thrownException.getMessage());

        verify(departmentRepository, times(1)).existsById(department.getId());
    }

    @Test
    public void updateDepartmentWithInvalidValuesShouldReturnBadRequest() throws IOException {
        HttpPut httpPut = new HttpPut(HttpUtils.BASE_ENDPOINT + "department");

        CloseableHttpClient client = HttpClientBuilder.create().build();

        httpPut.setEntity(new StringEntity(INVALID_JSON, ContentType.APPLICATION_JSON));

        CloseableHttpResponse response = client.execute(httpPut);

        int status = response.getStatusLine().getStatusCode();

        assertEquals(HttpStatus.BAD_REQUEST.value(), status);

        client.close();
        response.close();
    }

    @Test
    public void deleteDepartmentWithValidIdShouldReturnStatus200() throws NotFoundException {
        Department department = departmentList.get(0);

        when(departmentRepository.existsById(department.getId())).thenReturn(true);

        ResponseEntity<ResponseMessage> responseEntity = departmentController.deleteDepartmentById(department.getId());

        assertAll(
                () -> assertEquals(ResponseMessage.SUCCESSFULLY_DELETED, Objects.requireNonNull(responseEntity.getBody()).getMessage()),
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals(HttpStatus.OK.toString(), responseEntity.getBody().getStatus())
        );

        verify(departmentRepository, times(1)).existsById(department.getId());
    }

    @Test
    public void deleteDepartmentWithInvalidIdShouldReturnStatus404andThrowNotFoundException() {
        Department department = departmentList.get(0);

        when(departmentRepository.existsById(department.getId())).thenReturn(false);

        NotFoundException notFoundException = assertThrows(NotFoundException.class,
                () -> departmentController.deleteDepartmentById(department.getId()));

        assertEquals(DepartmentController.DEPARTMENT_NOT_FOUND, notFoundException.getMessage());
        verify(departmentRepository, times(1)).existsById(department.getId());
    }



}
